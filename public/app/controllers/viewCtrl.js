/**
 * Created by Reinis on 26.02.2016.
 */
(function () {
    "use strict";

    var app = angular.module("app", ["ui.bootstrap", "ngFileUpload", "angularResizable"]);

    app.directive('draggable', ['$document', function ($document) {
        return {

            scope: {
                exPos: "=",
                yPos: "="
            },
            link: function ($scope, element, attr) {
                var startX = $scope.exPos || 0;
                var startY = $scope.yPos || 0;
                var x = 0;
                var y = 0;

                $scope.$watch("exPos",
                    function (newVal, oldVal) {
                        if (newVal) {
                            if (newVal > 0) {
                                if (newVal < 530) {
                                    x = newVal;
                                }
                                else {
                                    x = 530;
                                }
                            }
                            else {
                                x = 0;
                            }
                            element.css({
                                left: x + 'px'
                            });
                        }
                    }, true);

                $scope.$watch("yPos",
                    function (newVal, oldVal) {
                        if (newVal) {
                            if (newVal > 0) {
                                if (newVal < 530) {
                                    y = newVal;
                                }
                                else {
                                    y = 530;
                                }
                            }
                            else {
                                y = 0;
                            }
                            element.css({
                                top: y + 'px'
                            });
                        }
                    }, true);

                element.css({
                    position: 'relative',
                    cursor: 'pointer'
                });

                element.on('mousedown', function (event) {
                    // Prevent default dragging of selected content
                    startX = event.pageX - x;
                    startY = event.pageY - y;
                    $document.on('mousemove', mousemove);
                    $document.on('mouseup', mouseup);
                });

                function mousemove(event) {
                    var possY = event.pageY - startY;
                    var possX = event.pageX - startX;
                    if (possY > 0) {
                        if (possY < 530) {
                            y = possY;
                        }
                        else {
                            y = 530;
                        }
                    }
                    else {
                        y = 0;
                    }
                    if (possX > 0) {
                        if (possX < 530) {
                            x = possX;
                        }
                        else {
                            x = 530;
                        }
                    }
                    else {
                        x = 0;
                    }
                    $scope.yPos = y;
                    $scope.exPos = x;
                    $scope.$apply();
                    element.css({
                        top: y + 'px',
                        left: x + 'px'
                    });
                }

                function mouseup() {
                    $document.off('mousemove', mousemove);
                    $document.off('mouseup', mouseup);
                }
            }
        };
    }]);

    app.controller("viewCtrl",
        ["$scope", "$rootScope", "Upload", "$timeout", "$http", "$uibModal",
            function ($scope, $rootScope, Upload, $timeout, $http, $uibModal) {


            $scope.$watch('files', function () {
                $scope.upload($scope.files);
            });
            $scope.$watch('file', function () {
                if ($scope.file != null) {
                    $scope.files = [$scope.file];
                }
            });

            $scope.images = [];
            $scope.orders = [];
            $scope.loadingBars = [];
            $scope.selectedOrder = {
                filename: "",
                x_pos: "",
                y_pos: "",
                height: "",
                width: ""
            };


            $scope.events = [];
            $scope.$on("angular-resizable.resizing", function (event, args) {
                if (args.height) {
                    $scope.activeOrder.height = args.height;
                }
                if (args.width) {
                    $scope.activeOrder.width = args.width;
                }
                //$scope.$apply();
                $scope.events.unshift(event);
            });

            // get initial images
            $http({
                method: 'GET',
                url: '/image'
            }).then(
                function (resp) {
                    for (var i = 0; i < resp.data.length; i++) {
                        $scope.images.push(resp.data[i]);
                        if (!$scope.selectedOrder) {
                            $scope.selectedOrder = {filename: resp.data[i].original_filename};
                        }
                    }

                }
            );

            // get initial orders
            $http({
                method: 'GET',
                url: '/order'
            }).then(
                function (resp) {
                    for (var i = 0; i < resp.data.length; i++) {
                        resp.data[i].$active = false;
                        $scope.orders.push(resp.data[i]);
                    }
                }
            );


            $scope.activateOrder = function (orderArray, order) {
                for (var i = 0; i < orderArray.length; i++) {
                    if (angular.equals(orderArray[i], order)) {
                        orderArray[i].$active = true;
                        $scope.originalOrder = orderArray[i];
                        $scope.activeOrder = angular.copy(orderArray[i]);
                        $scope.activateImage($scope.images, order.filename);
                    } else {
                        orderArray[i].$active = false;
                    }
                }
            };

            var getDiff = function (order1, order2) {
                var diffOrder = {};
                angular.forEach(order1,
                    function (value1, key1) {
                        if (key1.charAt(0) !== '$') {
                            if (!angular.isUndefined(order2[key1])) {
                                if (!angular.equals(order2[key1], value1)) {
                                    diffOrder[key1] = order2[key1];
                                }
                            }
                            else {
                                throw new Error("no such " + key1 + " on order2\n" + angular.toJson(order1) + "\n" + angular.toJson(order2));
                            }
                        }
                    });
                if (angular.equals(diffOrder, {})) {
                    return null;
                } else {
                    return diffOrder;
                }
            }

            $scope.$watch("originalOrder",
                function (newValues, oldValues) {
                    if (!newValues || !$scope.activeOrder) {
                        return;
                    }
                    $scope.diffOrder = getDiff(newValues, $scope.activeOrder);
                    if ($scope.diffOrder) {
                        $scope.changeCount = Object.keys($scope.diffOrder).length;
                    }

                }, true);

            $scope.$watch("activeOrder",
                function (newValues, oldValues) {
                    if (!newValues || !$scope.originalOrder) {
                        return;
                    }
                    $scope.diffOrder = getDiff($scope.originalOrder, newValues);
                    if ($scope.diffOrder) {
                        $scope.changeCount = Object.keys($scope.diffOrder).length;
                    }
                }, true);


            $scope.createEmptyOrder = function () {
                var order = {
                    filename: "",
                    x_pos: 0,
                    y_pos: 0,
                    height: 160,
                    width: 200,
                    created_at: new Date().toISOString(),
                    notSaved: true
                };
                $scope.orders.push(order);
                $scope.activateOrder($scope.orders, order);
            };

            $scope.activateImage = function (imgArray, filename) {
                angular.forEach(imgArray,
                    function (img) {
                        if (angular.equals(img.original_filename, filename)) {
                            img.$active = true;
                            $scope.activeOrder.$resizedFileAddr = img.resized_file_address;
                            $scope.activeOrder.filename = img.original_filename;
                        } else {
                            img.$active = false;
                        }
                    });
            };

            $scope.upload = function (files) {
                if (files && files.length) {
                    for (var i = 0; i < files.length; i++) {
                        var file = files[i];
                        if (!file.$error) {
                            uploadFile(file);
                        }
                    }
                }
            };

            $scope.activeOrdersAny = function () {
                for (var i = 0; i < $scope.orders.length; i++) {
                    if ($scope.orders[i].$active) {
                        return true;
                    }
                }
                return false;
            };

            var uploadFile = function (file) {
                {
                    var id = Math.random();
                    var tab = {
                        value: 0,
                        type: "info",
                        height: "120",
                        width: "160",
                        name: file.name,
                        id: id
                    };
                    $scope.loadingBars.push(tab);

                    Upload.upload({
                        url: 'http://localhost/image',
                        data: {
                            file: file
                        }
                    }).then(
                        function (resp) {
                            $scope.images.push(resp.data);
                            for (var i = 0; i < $scope.loadingBars.length; i++) {
                                if ($scope.loadingBars[i].id === id) {
                                    $scope.loadingBars[i].value = 100;
                                    $scope.loadingBars[i].type = "success";
                                    removeFromBarsAfterDelay($scope.loadingBars[i]);
                                }
                            }

                        },
                        function (error) {
                            for (var i = 0; i < $scope.loadingBars.length; i++) {
                                if ($scope.loadingBars[i].id === id) {
                                    $scope.loadingBars[i].value = 100;
                                    $scope.loadingBars[i].type = "error";
                                }
                            }
                        },
                        function (evt) {
                            for (var i = 0; i < $scope.loadingBars.length; i++) {
                                if ($scope.loadingBars[i].id === id) {
                                    $scope.loadingBars[i].value = parseInt(100.0 * evt.loaded / evt.total);
                                }
                            }
                        });
                }
            };

            var removeFromBarsAfterDelay = function (tab) {
                $timeout(function () {
                    $scope.loadingBars.splice($scope.loadingBars.indexOf(tab), 1)
                }, 2000);
            }

            $scope.saveNewOrder = function (order) {
                $scope.orderIsSaving = true;
                $http({
                    method: 'POST',
                    url: '/order',
                    data: order
                }).then(
                    function (resp) {
                        $scope.orders.splice($scope.orders.indexOf(order), 1);
                        $scope.orders.push(resp.data);
                        $scope.activateOrder($scope.orders, resp.data);
                        $scope.orderIsSaving = false;
                    },
                    function (error) {
                        $scope.orderIsSaving = false;
                        throw new Error("failed to save new order", error);
                    }
                );
            }

            $scope.saveEditedOrder = function (order) {
                $scope.orderIsSaving = true;
                $http({
                    method: 'PUT',
                    url: '/order/' + order.id,
                    data: order
                }).then(
                    function (resp) {
                        $scope.originalOrder = angular.copy(order);
                        $scope.orderIsSaving = false;
                    },
                    function (error) {
                        $scope.orderIsSaving = false;
                        throw new Error("failed to save edited order", error);
                    }
                );
            }

            $scope.orderDeleteButton = function (order) {
                $http({
                    method: 'DELETE',
                    url: '/order/' + order.id,
                }).then(
                    function (resp) {
                        var index = $scope.orders.indexOf(order);

                        if (index < 0) {
                            throw new Error("delete order: order not found", order);
                        }
                        var active = $scope.orders[index].$active;
                        $scope.orders.splice(index, 1);
                        if (active){
                            if($scope.orders[0]){
                                $scope.activateOrder($scope.orders, $scope.orders[0]);
                            }else{
                                $scope.selectedOrder = null;
                            }
                        }
                    },
                    function (error) {
                        throw new Error("delete order: failed to delete order", error);
                    }
                );
            }

            $scope.imgDeleteButton = function (img) {
                var modalInstance = $uibModal.open({
                    animation: true,
                    controller: "ModalInstanceCtrl",
                    templateUrl: 'myModalContent.html',
                    size: "sm"
                });

                modalInstance.result.then(
                    function () {
                        // executed
                        $http({
                            method: 'DELETE',
                            url: '/image/' + img.id,
                        }).then(
                            function (resp) {
                                var index = $scope.images.indexOf(img);
                                if (index < 0) {
                                    throw new Error("delete image: image not found", img);
                                }
                                $scope.images.splice(index, 1);
                                for (var i = 0; i < $scope.orders.length; i++) {
                                    if($scope.orders[i].filename === img.original_filename){
                                        var active = $scope.orders[i].$active;
                                        $scope.orders.splice(i, 1);
                                        if (active){
                                            if($scope.orders[0]){
                                                $scope.activateOrder($scope.orders, $scope.orders[0]);
                                            }else{
                                                $scope.selectedOrder = null;
                                            }
                                        }
                                    }
                                }
                            },
                            function (error) {
                                throw new Error("delete image: failed to delete image", error);
                            }
                        );
                }, function () {
                    // cancelled
                });



            };


        }]).controller('ModalInstanceCtrl', function ($scope, $uibModalInstance) {

        $scope.ok = function () {
            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });
})();
