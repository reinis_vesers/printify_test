<?php

namespace App\Http\Controllers;


use App\Http\Requests;

use App\Orders;
use App\pictures;

use Image;
use Request;
use Storage;
use File;
use Response;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entries = pictures::All();

        return Response::make($entries, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // get image
        $image = Request::file('file');
        $extension = $image->getClientOriginalExtension();

        // save original img on disk
        if (!File::exists(storage_path() . '/app/originalImages')) {
            File::makeDirectory(storage_path() . '/app/originalImages');
        }
        $originalFilePath = 'originalImages/' . $image->getFilename() . '.' . $extension;
        $fileSavedSuccessfully = Storage::disk('local')->put($originalFilePath, File::get($image));

        if (!$fileSavedSuccessfully) {
            return Response::make($image->getClientOriginalName(), 500);
        }


        // resize image to no bigger 400x400 px
        $originalFile = Storage::disk('local')->get($originalFilePath);
        $img = Image::make($originalFile);
        $img->resize(400, 400, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        // save resized img on disk
        if (!File::exists(public_path() . '/resizedImages')) {
            File::makeDirectory(public_path() . '/resizedImages');
        }
        $resizedFileSavePath = public_path() . '/resizedImages/' . $image->getFilename() . '.' . $extension;

        $fileResizedSuccessfully = $img->save($resizedFileSavePath);
        if (!$fileResizedSuccessfully) {
            return Response::make($image->getClientOriginalName(), 500);
        }

        // save a db entry
        $entry = new Pictures();
        $entry->mime = $image->getClientMimeType();
        $entry->original_filename = $image->getClientOriginalName();
        $entry->filename = $image->getFilename() . '.' . $extension;
        $entry->resized_filename = $resizedFileSavePath;
        $entry->resized_file_address = '/resizedImages/' . $entry->filename;

        $entry->save();

        return Response::make($entry, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $entry = pictures::where('id', '=', $id)->firstOrFail();
        $file = storage_path() . '/app/originalImages/' . $entry->filename;//Storage::disk('local')->get('originalImages/' . $entry->filename);

        return Response::download($file, $entry->original_filename, ['Content-Type' => $entry->mime]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $entry = Pictures::where("id", "=", $id)->FirstOrFail();
        $orders = Orders::where("filename", "=", $entry->original_filename);
        $orders->delete();

        $originalFilePath = 'originalImages/' . $entry->filename;
        if(!Storage::disk('local')->delete($originalFilePath)){
            return Response::make($id, 500);
        }
        if(!File::delete('../public/resizedImages/' . $entry->filename)){
            return Response::make($id, 500);
        }

        $entry->delete();
        return Response::make($id, 200);
    }
}
