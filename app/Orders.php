<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $fillable = array('filename', 'x_pos', 'y_pos','height','width');
}
