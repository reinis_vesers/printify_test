<!DOCTYPE html>
<html ng-app="app">
<head>
    <title>Upload</title>
    <link href="<?= asset('css/bootstrap.css') ?>" rel="stylesheet">
    <link href="<?= asset('css/main.css') ?>" rel="stylesheet">
    <link href="<?= asset('css/font-awesome.css') ?>" rel="stylesheet">
    <link href="<?= asset('app/lib/angular-resizable.css') ?>" rel="stylesheet">

</head>
<body ng-controller="viewCtrl">
<div class="container-fluid" style="max-width:600px">
    <div class="panel panel-info">
        <div class="panel-body">

            <div class="row">
                <div class="col-xs-12">
                    <h1>Your orders</h1>
                    <table class="table table-hover">
                        <thead>
                        <th>
                            <label ng-bind="'filename'"></label>
                        </th>
                        <th>
                            <label ng-bind="'height x width'"></label>
                        </th>
                        <th>
                            <label ng-bind="'x and y'"></label>
                        </th>
                        <th>
                            <label ng-bind="'Created'"></label>
                        </th>
                        <th>
                            <label ng-bind="'Last updated'"></label>
                        </th>
                        <th>

                        </th>
                        </thead>
                        <tbody>
                        <tr ng-repeat="order in orders"
                            ng-class="{'info': order.$active}"
                            ng-click="activateOrder(orders, order)">
                            <td class="col-xs-8">
                                <span ng-bind="order.filename"></span>
                            </td>
                            <td class="col-xs-4">
                                <span ng-if="order.height && order.width"
                                      ng-bind="order.height + 'x' + order.width"></span>
                            </td>

                            <td class="col-xs-4">
                                <spang-if="order.x_pos && order.y_pos"n
                                    ng-bind="order.x_pos + 'x' + order.y_pos"></span>
                            </td>
                            <td class="col-xs-4">
                                <span ng-bind="order.created_at"></span>
                            </td>
                            <td class="col-xs-4">
                                <span ng-bind="order.updated_at"></span>
                            </td>
                            <td>
                                <i class="fa fa-3x col-xs-12 text-center"
                                   ng-class="{'fa-trash-o': !order.$DeleteMouseDown, 'fa-trash': order.$DeleteMouseDown}"
                                   ng-click="orderDeleteButton(order)"
                                   ng-mousedown="order.$DeleteMouseDown = true"
                                   ng-mouseup="order.$DeleteMouseDown = false"></i>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <a ng-click="createEmptyOrder()">
                                    <div>
                                        <span ng-bind="'Create a new order'"></span>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        </tbody>

                    </table>

                </div>
            </div>

            <hr/>

            <div ng-show="activeOrder">
                <div class="row">
                    <div class="col-xs-12">
                        <h1>Order editor</h1>
                    </div>
                </div>


                <div class="row">
                    <div class="col-xs-12">
                        <div
                            ng-style="{'background-image': 'url(<?= asset('shirt.png') ?>)', 'width': '530px', 'height':'530px'}">
                            <div draggable
                                 ex-pos="activeOrder.x_pos"
                                 y-pos="activeOrder.y_pos">
                                <div ng-if="activeOrder.$resizedFileAddr"
                                     resizable
                                     r-directions="['right', 'bottom', 'left', 'top']"
                                     r-centered-x="true"
                                     r-centered-y="true"
                                     r-width="activeOrder.width"
                                     r-height="activeOrder.height"
                                     ng-style="{'background-image': 'url(' + activeOrder.$resizedFileAddr + ')'}"
                                     style="width:530px; height:530px; background-size: 100% 100%;">

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <span
                            ng-bind="'Choose any of these images for your order. If you want to put other images on the order, just add them by dragging and dropping them in this box or clicking in the box to select new images from your computer.'"></span>
                    </div>
                </div>

                <!-- Image upload dropzone -->
                <div class="row">
                    <div class="col-xs-12">
                        <div ngf-drop ngf-select
                             ng-model="files"
                             class="upload-drop-zone"
                             ngf-drag-over-class="'drop'"
                             ngf-multiple="true"
                             ngf-allow-dir="false"
                             accept="image/*"
                             ngf-pattern="'image/*'"
                             ngf-fix-orientation="true"
                             ngf-capture="'camera'">
                            <div ng-if="images.length === 0"
                                 class="row">
                                <h2 class="fa fa-upload fa-4x col-xs-12 text-center"></h2>
                                <h2 class="col-xs-24 text-center"
                                    ng-bind="'drop files here or click to upload files'"></h2>
                            </div>
                            <!-- actual image objects-->
                            <div class="row">
                                <div class="animate-repeat form-group"
                                     name="images"
                                     ng-repeat="img in images">
                                    <div class="clearfix" ng-if="$index % 3 == 0"></div>
                                    <div class="col-xs-4"
                                         ng-click="$event.stopPropagation();">
                                        <div class="img-container"
                                             ng-mouseenter="img.$mouseOn = true"
                                             ng-mouseleave="img.$mouseOn = false"
                                             ng-click="activateImage(images, img.original_filename)">
                                            <img class="img-thumbnail"
                                                 ng-class="{'bg-primary': img.$active}"
                                                 ng-src="{{img.resized_file_address}}"
                                                 alt="{{img.original_filename}}">
                                            <div class="animate-show"
                                                 ng-show="img.$mouseOn">
                                                <div class="row">
                                                    <h2 class="fa fa-3x col-xs-6 text-center"
                                                        ng-class="{'fa-trash': !img.$imgDeleteMouseDown, 'fa-trash-o': img.$imgDeleteMouseDown}"
                                                        ng-click="imgDeleteButton(img);$event.stopPropagation();"
                                                        ng-mousedown="img.$imgDeleteMouseDown = true"
                                                        ng-mouseup="img.$imgDeleteMouseDown = false"
                                                        uib-tooltip="delete image"
                                                        tooltip-placement="top"
                                                        tooltip-trigger="mouseenter"
                                                        tooltip-popup-delay="1000"></h2>
                                                    <a ng-href="{{'image/' + img.id}}">
                                                        <h2 class="fa fa-download fa-3x col-xs-6 text-center"
                                                            uib-tooltip="download image in full resolution"
                                                            tooltip-placement="top"
                                                            tooltip-trigger="mouseenter"
                                                            tooltip-popup-delay="1000"></h2>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <span ngf-no-file-drop
                              ng-bind="'your browser is too old for drag and drop. Please use a newer browser'"></span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <uib-progress ng-show="loadingBars.length > 0">
                            <uib-bar ng-repeat="bar in loadingBars track by $index"
                                     value="bar.value"
                                     type="{{bar.type}}">
                            <span ng-if="bar.type === 'success'">
                                <span ng-bind="'file'"></span>
                                <span ng-bind="bar.name"></span>
                                <span ng-bind="'successfully uploaded.'"></span>
                            </span>
                            <span ng-if="bar.type === 'info'">
                                <span ng-bind="'uploading file'"></span>
                                <span ng-bind="bar.name"></span>

                                <span ng-hide="bar.value < 5">
                                    <span ng-bind="' ' + bar.value + '%'"></span>
                                </span>
                            </span>
                            <span ng-if="bar.type === 'error'">
                                <span ng-bind="'file'"></span>
                                <span ng-bind="bar.name"></span>
                                <span ng-bind="'upload failed.'"></span>
                            </span>

                            </uib-bar>
                        </uib-progress>
                    </div>
                </div>

                <div ng-if="diffOrder">
                    <hr/>
                    <div class="row">
                        <div class="col-xs-12">
                            <h4>
                                <span ng-bind="'order has'"></span>
                                <span ng-bind="changeCount"></span>
                                <span ng-bind="'unsaved changes.'"></span>
                            </h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3"
                             ng-if="diffOrder.filename">
                            <label ng-bind="'filename'"></label>
                            <span ng-bind="diffOrder.filename"></span>
                        </div>
                        <div class="col-xs-3"
                             ng-if="diffOrder.x_pos">
                            <label ng-bind="'x coordinate'"></label>
                            <span ng-bind="activeOrder.x_pos"></span>
                        </div>
                        <div class="col-xs-3"
                             ng-if="diffOrder.y_pos">
                            <label ng-bind="'y coordinate'"></label>
                            <span ng-bind="activeOrder.y_pos"></span>
                        </div>
                        <div class="col-xs-3"
                             ng-if="diffOrder.height">
                            <label ng-bind="'height'"></label>
                            <span ng-bind="activeOrder.height"></span>
                        </div>
                        <div class="col-xs-3"
                             ng-if="diffOrder.width">
                            <label ng-bind="'width'"></label>
                            <span ng-bind="activeOrder.width"></span>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <uib-alert ng-if="!activeOrder.filename"
                            <uib-alert ng-if="!activeOrder.filename"
                                       type="error">
                                <span ng-bind="'an order must have an image on it.'"></span>
                            </uib-alert>
                            <button ng-if="activeOrder.filename && !activeOrder.notSaved"
                                    class="btn btn-default pull-right"
                                    ng-click="saveEditedOrder(activeOrder)">
                        <span ng-bind="'save changes to this order'"
                              ng-if="!orderIsSaving"></span>
                                <i class="fa fa-circle-o-notch fa-spin"
                                   ng-if="orderIsSaving"></i>
                            </button>
                            <button ng-if="activeOrder.filename && activeOrder.notSaved"
                                    class="btn btn-default pull-right"
                                    ng-click="saveNewOrder(activeOrder)">
                        <span ng-bind="'create this order'"
                              ng-if="!orderIsSaving"></span>
                                <i class="fa fa-circle-o-notch fa-spin"
                                   ng-if="orderIsSaving"></i>
                            </button>

                        </div>
                    </div>

                </div>
            </div>


        </div>


        <script src="<?= asset('app/lib/angular.js') ?>"></script>
        <script src="<?= asset('app/lib/ui-bootstrap-tpls-1.1.2.js') ?>"></script>
        <script src="<?= asset('app/lib/ng-file-upload.js') ?>"></script>
        <script src="<?= asset('app/lib/angular-resizable.js') ?>"></script>

        <script src="<?= asset('app/controllers/viewCtrl.js') ?>"></script>

</body>
</html>


<div>
    <script type="text/ng-template" id="myModalContent.html">
        <div class="modal-body">
            <span ng-bind="'are you sure you want to delete this image? This action will also delete all of the related orders.'"></span>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" type="button" ng-click="ok()">OK</button>
            <button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button>
        </div>
    </script>
